// External paths
use thiserror::Error;

// Crate paths
use crate::core::os::error::OsError;

#[derive(Error, Debug)]
pub enum UsbGadgetMainCleanError {
    #[error("UsbGadgetMainCleanError: cannot clean previously setup main gadget resource")]
    CannotCleanGadget {
        #[from]
        source: OsError,
    },
}

#[derive(Error, Debug)]
pub enum UsbGadgetMainSetupError {
    #[error("UsbGadgetMainSetupError: cannot setup main gadget")]
    Setup {
        #[from]
        source: OsError,
    },
}
