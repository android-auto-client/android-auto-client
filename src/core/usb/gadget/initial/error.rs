// External paths
use thiserror::Error;

// Crate paths
use crate::core::os::error::OsError;

#[derive(Error, Debug)]
pub enum UsbGadgetInitialCleanError {
    #[error("UsbGadgetInitialCleanError: cannot clean previously setup initial gadget resource")]
    CannotCleanGadget {
        #[from]
        source: OsError,
    },
}

#[derive(Error, Debug)]
pub enum UsbGadgetInitialSetupError {
    #[error("UsbGadgetInitialSetupError: cannot setup initial gadget")]
    Setup {
        #[from]
        source: OsError,
    },
}
