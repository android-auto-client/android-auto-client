// Standard paths
use std::path::Path;
use std::str;

// External paths
use hex;
use log::debug;
use log::info;
use pretty_hex::*;

// Crate paths
use crate::core::os;
use error::UsbGadgetInitialCleanError;
use error::UsbGadgetInitialSetupError;

// Crate modules
pub mod error;

/// Clean any previous gadget setup
///
/// # Errors
///
/// * cannot clean gadget. System is in unexpected state. Try to reboot.
pub fn clean() -> Result<(), UsbGadgetInitialCleanError> {
    debug!("cleaning initial usb gadget if any");

    // Disable gadget on hardwware usb controller if running
    let path = "/sys/kernel/config/usb_gadget/initial_state/UDC";
    if Path::new(path).exists() {
        os::write(path, "")?;
    }

    let path = "/sys/kernel/config/usb_gadget/initial_state/configs/c0.1/massstorage_initial";
    if Path::new(path).exists() {
        os::remove_file(path)?;
    }

    let path = "/sys/kernel/config/usb_gadget/initial_state/configs/c0.1/ffs.ffs_initial";
    if Path::new(path).exists() {
        os::remove_file(path)?;
    }

    let path = "/sys/kernel/config/usb_gadget/initial_state/configs/c0.1/loopback_initial";
    if Path::new(path).exists() {
        os::remove_file(path)?;
    }

    let path = "/sys/kernel/config/usb_gadget/initial_state/configs/c0.1/strings/0x409";
    if Path::new(path).exists() {
        os::remove_dir(path)?;
    }

    let path = "/sys/kernel/config/usb_gadget/initial_state/configs/c0.1";
    if Path::new(path).exists() {
        os::remove_dir(path)?;
    }

    let path =
        "/sys/kernel/config/usb_gadget/initial_state/functions/mass_storage.massstorage_initial";
    if Path::new(path).exists() {
        os::remove_dir(path)?;
    }

    let path = "/sys/kernel/config/usb_gadget/initial_state/functions/ffs.ffs_initial";
    if Path::new(path).exists() {
        os::remove_dir(path)?;
    }

    let path = "/sys/kernel/config/usb_gadget/initial_state/strings/0x409";
    if Path::new(path).exists() {
        os::remove_dir(path)?;
    }

    let path = "/sys/kernel/config/usb_gadget/initial_state";
    if Path::new(path).exists() {
        os::remove_dir(path)?;
    }

    let mounts = os::read_to_string("/proc/mounts")?;
    let path = "/tmp/android-auto-client/loopback_initial";
    if mounts.contains(path) {
        os::umount(path)?;
    }

    Ok(())
}

/// Setup initial gadget configuration for cdrom.
///
/// Client has to introduce itself somehow to Android Auto server so pretend to be mass storage device cdrom.
///
/// # Errors
///
/// * cannot setup properly gadget in kernel config file system. Ensure running kernel version supports configfs well enought.
/// * cannot assign created gadget function to gadget configuration. Ensure running kernel version supports configfs well enought.
fn cdrom_setup() -> Result<(), UsbGadgetInitialSetupError> {
    debug!("setting usb initial gadget: cdrom");

    os::create_dir_all(
        "/sys/kernel/config/usb_gadget/initial_state/functions/mass_storage.massstorage_initial/",
    )?;

    os::write("/sys/kernel/config/usb_gadget/initial_state/functions/mass_storage.massstorage_initial/stall", "0")?;

    os::write("/sys/kernel/config/usb_gadget/initial_state/functions/mass_storage.massstorage_initial/lun.0/cdrom", "1")?;

    os::write("/sys/kernel/config/usb_gadget/initial_state/functions/mass_storage.massstorage_initial/lun.0/ro", "0")?;

    os::write("/sys/kernel/config/usb_gadget/initial_state/functions/mass_storage.massstorage_initial/lun.0/nofua", "0")?;

    os::write("/sys/kernel/config/usb_gadget/initial_state/functions/mass_storage.massstorage_initial/lun.0/removable", "1")?;

    os::create_dir_all("/tmp/android-auto-client")?;

    // File is required to have some size
    // Otherwise setting it as content for mass storage fails
    let lun0_content = "0".repeat(1024 * 1024);

    os::write("/tmp/android-auto-client/lun0", lun0_content.as_str())?;

    os::write("/sys/kernel/config/usb_gadget/initial_state/functions/mass_storage.massstorage_initial/lun.0/file", "/tmp/android-auto-client/lun0")?;

    os::symlink(
        "/sys/kernel/config/usb_gadget/initial_state/functions/mass_storage.massstorage_initial",
        "/sys/kernel/config/usb_gadget/initial_state/configs/c0.1/massstorage_initial",
    )?;

    Ok(())
}

/// Expose gadget usb control endpoint to exchange messages with Android Auto server.
///
/// # Errors
///
/// * cannot setup properly gadget in kernel config file system. Ensure running kernel version supports configfs well enought.
/// * cannot expose gadget usb control endpoint. Ensure running kernel version supports functionfs well enought.
fn functionfs_setup() -> Result<os::FileHandle, UsbGadgetInitialSetupError> {
    debug!("setting usb initial gadget: functionfs");

    os::create_dir_all("/sys/kernel/config/usb_gadget/initial_state/functions/ffs.ffs_initial")?;

    os::create_dir_all("/tmp/android-auto-client/loopback_initial")?;

    os::mount_functionfs("ffs_initial", "/tmp/android-auto-client/loopback_initial")?;

    os::symlink(
        "/sys/kernel/config/usb_gadget/initial_state/functions/ffs.ffs_initial",
        "/sys/kernel/config/usb_gadget/initial_state/configs/c0.1/ffs.ffs_initial",
    )?;

    let mut ep0 = os::open_read_write("/tmp/android-auto-client/loopback_initial/ep0")?;

    // Usb configuration description for this device
    // Kernel will handle it and exchange messages on usb bus accordingly
    // Input is hardcoded and checked already
    // It will never panic (assuming compatible kernel version...)
    let frame =
        hex::decode("03000000260000004300000001000000010000000904000000ffff00010904000000ffff0001")
            .unwrap();

    os::write_file_handle(&mut ep0, &frame)?;

    // Usb configuration description for this device
    // Kernel will handle it and exchange messages on usb bus accordingly
    // Input is hardcoded and checked already
    // It will never panic (assuming compatible kernel version...)
    let frame = hex::decode("020000002e00000001000000010000000904416e64726f6964204163636573736f727920496e7465726661636500").unwrap();

    os::write_file_handle(&mut ep0, &frame)?;

    Ok(ep0)
}

/// Enable prepared usb gadget configuration on usb gadget controller hardware.
///
/// # Errors
///
/// * cannot assign gadget configuration to gadget controller. Ensure there is not any other gadget configured already which uses same gadget controller.
fn enable(controller_usb_gadget: &str) -> Result<(), UsbGadgetInitialSetupError> {
    debug!("enabling usb initial gadget");

    os::write(
        "/sys/kernel/config/usb_gadget/initial_state/UDC",
        controller_usb_gadget,
    )?;

    Ok(())
}

/// Read usb host configuration.
/// Usb host should send messages following Android Auto protocol.
/// If host does not comply with protocol, then it will not send enough expected messages.
/// When expected messages are not coming, then this function hungs waiting for them indefinitely.
///
/// # Errors
///
/// * see [os::read_file_handle]
fn read(ep: &mut os::FileHandle) -> Result<(), UsbGadgetInitialSetupError> {
    debug!("reading usb initial gadget endpoint 0");

    // Prepare buffer on stack
    let mut buffer = [0; 48];

    // Read frame
    let bytes = os::read_file_handle(ep, &mut buffer)?;
    debug!(
        "reading usb initial gadget endpoint 0: frame 1: {:?}",
        (&buffer[..bytes]).hex_dump()
    );

    // Read frame
    let bytes = os::read_file_handle(ep, &mut buffer)?;
    debug!(
        "reading usb initial gadget endpoint 0: frame 2: {:?}",
        (&buffer[..bytes]).hex_dump()
    );

    info!("only usb host which supports Android Auto protocol will send further expected messages");
    info!("if usb host does not comply with protocol, then expected messages will not be send and client will hung waiting for these messages forever");

    // Read frame
    let bytes = os::read_file_handle(ep, &mut buffer)?;
    debug!(
        "reading usb initial gadget endpoint 0: frame 3: {:?}",
        (&buffer[..bytes]).hex_dump()
    );

    // Send response after first 3 frames
    let response = [2, 0];
    os::write_file_handle(ep, &response)?;

    // Read rest of messages
    // TODO parse messages and look for Android Auto messages
    // If detected, then proceed to main gadget
    loop {
        // Read frame
        let bytes = os::read_file_handle(ep, &mut buffer)?;

        // Exit loop when all messages are read
        if bytes == 0 {
            break;
        }

        // Dump frame as log
        debug!(
            "reading usb initial gadget endpoint 0: frame: {:?}",
            (&buffer[..bytes]).hex_dump()
        );
    }

    info!("all messages from endpoint 0 are read");

    Ok(())
}

/// Setup initial gadget
///
/// # Errors
///
/// * cannot properly setup gadget. Ensure that running kernel properly supports libcomposite, configfs, functionfs.
/// * see [cdrom_setup].
/// * see [functionfs_setup].
/// * see [enable].
pub fn setup(controller_usb_gadget: &str) -> Result<(), UsbGadgetInitialSetupError> {
    debug!("setting usb initial gadget");

    os::create_dir_all("/sys/kernel/config/usb_gadget/initial_state")?;

    // Linux Foundation
    os::write(
        "/sys/kernel/config/usb_gadget/initial_state/idVendor",
        "0x1d6b",
    )?;

    // Multifunction Composite Gadget
    os::write(
        "/sys/kernel/config/usb_gadget/initial_state/idProduct",
        "0x0104",
    )?;

    // Lanugage english
    os::create_dir_all("/sys/kernel/config/usb_gadget/initial_state/strings/0x409")?;

    os::write(
        "/sys/kernel/config/usb_gadget/initial_state/strings/0x409/serialnumber",
        "0123456789",
    )?;

    os::write(
        "/sys/kernel/config/usb_gadget/initial_state/strings/0x409/product",
        "Android Auto Client",
    )?;

    os::write(
        "/sys/kernel/config/usb_gadget/initial_state/strings/0x409/manufacturer",
        "ACME",
    )?;

    os::create_dir_all("/sys/kernel/config/usb_gadget/initial_state/configs/c0.1")?;

    // Lanugage english
    os::create_dir_all("/sys/kernel/config/usb_gadget/initial_state/configs/c0.1/strings/0x409")?;

    os::write(
        "/sys/kernel/config/usb_gadget/initial_state/configs/c0.1/strings/0x409/configuration",
        "Mass Storage",
    )?;

    os::write(
        "/sys/kernel/config/usb_gadget/initial_state/configs/c0.1/MaxPower",
        "48",
    )?;

    cdrom_setup()?;

    // Local scope for endpoint 0
    {
        // Endpoint cannot be closed before enabling gadget
        let mut ep0 = functionfs_setup()?;

        enable(controller_usb_gadget)?;

        read(&mut ep0)?;
    }

    Ok(())
}
