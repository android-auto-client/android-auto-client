// Crate paths
use crate::core::usb;

// Declare modules
mod core {
    pub mod os;
    pub mod usb;
}

pub mod error;

// Main program logic
pub fn run() -> Result<(), error::AndroidAutoError> {
    usb::setup()?;

    Ok(())
}
